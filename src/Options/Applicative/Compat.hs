module Options.Applicative.Compat
  ( module Options.Applicative
  , strArgument
  ) where

import Options.Applicative hiding (strArgument)
import Data.String (IsString, fromString)
import qualified Options.Applicative as A

strArgument :: (IsString s) => Mod ArgumentFields String -> Parser s
strArgument m = fromString <$> A.strArgument m
