{-# LANGUAGE CPP #-}
module Data.Text.Compat
  ( module Data.Text
#if !MIN_VERSION_text(1,2,3)
  , unsnoc
#endif
  ) where
import Prelude
import qualified Prelude as P
import Data.Text
import qualified Data.Text as T 

-- This compatibility version is deliberately implemented in most
-- simple way possible. Using 'String'.
#if !MIN_VERSION_text(1,2,3)
unsnoc :: Text -> Maybe (Text, Char)
unsnoc txt =
  case T.unpack txt of
    [] -> Nothing
    s  -> Just (T.pack $ P.init s, P.last s)
#endif


