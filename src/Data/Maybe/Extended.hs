module Data.Maybe.Extended
  ( module Data.Maybe
  , maybeToAlternative
  ) where
import Control.Applicative (Alternative, empty)
import Data.Maybe

-- | Convert 'Maybe' value into arbitrary 'Alternative' instance,
-- mapping Nothing into 'empty'.
maybeToAlternative :: (Alternative f) => Maybe a -> f a
maybeToAlternative = maybe empty pure
