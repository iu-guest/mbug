ghcid:
	ghcid -o .git/error.txt -c 'stack repl --ghci-options=-fno-code'

reformat:
	find src -name '*.hs' -exec stylish-haskell -c aux/config/stylish.yaml -i {} \;

tags:
	find src -name '*.hs' | xargs hothasktags > tags

.PHONY: ghcid reformat tags
